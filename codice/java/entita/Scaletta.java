package entita;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="SCALETTA")
public class Scaletta {
	
	@Id
	@Column(name="NOME")
	private String nome;
	
	@Column(name="NUMERO_BRANI")
	private int numero_brani;
	
	@OneToMany(mappedBy ="scaletta" , cascade=CascadeType.ALL,orphanRemoval=true)
	private List<Brano> canzone;
	
	

	public Scaletta() {}
	public Scaletta(String nome, int numero_brani) {
		this.nome = nome;
		this.numero_brani = numero_brani;
		this.canzone =new ArrayList<Brano>(numero_brani);
	}
	
	public Scaletta(String nome, int numero_brani, List<Brano> canzone) {
		this.nome = nome;
		this.numero_brani = numero_brani;
		this.canzone = canzone;
	}


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getNumero_brani() {
		return numero_brani;
	}

	public void setNumero_brani(int numero_brani) {
		this.numero_brani = numero_brani;
	}

	public List<Brano> getCanzone() {
		return canzone;
	}

	public void setCanzone(List<Brano> canzone) {
		this.canzone = canzone;
	}
	
	public void addBrano(Brano brano) {
		if(!canzone.contains(brano)) {
			canzone.add(brano);
			brano.setScaletta(this);
		}
	}
	
	public void removeBrano(Brano brano) {
		if(canzone.contains(brano)) {
			canzone.remove(brano);
			//per eliminare completamente la relazione con scaletta
			brano.setScaletta(null);
			
		}
	}
	
	@Override
	public String toString() {
		return "Scaletta [nome=" + nome + ", numero_brani=" + numero_brani + ", canzone=" + canzone.toString() + "]";
	}
	
	
}
